package org.example.planet;

import org.example.obstacle.Obstacle;
import org.example.obstacle.ObstaclePosition;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PlanetShould {
    @Test
    void initialize_grid() {
        Planet planet = new Planet(4);

        assertThat(planet.getGrid()).isEqualTo(new int[4][4]);
    }

    @Test
    void initialize_planet_with_obstacle() {
        ObstaclePosition pos1 = new ObstaclePosition(0, 0);
        Obstacle o1 = new Obstacle(pos1);
        ObstaclePosition pos2 = new ObstaclePosition(1, 1);
        Obstacle o2 = new Obstacle(pos2);
        ObstaclePosition pos3 = new ObstaclePosition(2, 2);
        Obstacle o3 = new Obstacle(pos3);

        List<Obstacle> obstacles = List.of(o1, o2, o3);

        Planet planet = new Planet(4, obstacles);

        assertThat(planet.countObstacles()).isEqualTo(3);
    }

    @Test
    void only_have_obstacle_with_valid_position() {
        ObstaclePosition pos1 = new ObstaclePosition(0, 0);
        Obstacle o1 = new Obstacle(pos1);
        ObstaclePosition pos2 = new ObstaclePosition(1, 1);
        Obstacle o2 = new Obstacle(pos2);
        ObstaclePosition pos3 = new ObstaclePosition(7, 2);
        Obstacle o3 = new Obstacle(pos3);

        List<Obstacle> obstacles = List.of(o1, o2, o3);

        Planet planet = new Planet(4, obstacles);

        assertThat(planet.countObstacles()).isEqualTo(2);
    }
}
