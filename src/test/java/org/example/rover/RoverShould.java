package org.example.rover;

import org.example.obstacle.Obstacle;
import org.example.obstacle.ObstaclePosition;
import org.example.planet.Planet;
import org.example.position.Position;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class RoverShould {

    @Test
    void initialize_position() {
        RoverPosition position = new RoverPosition(0, 1);

        Rover rover = new Rover(position);

        assertThat(rover.getPosition()).isEqualTo(position);
    }

    @Test
    void initialize_planet() {
        Planet planet = new Planet(4);

        Rover rover = new Rover(planet);

        assertThat(rover.getPlanet()).isEqualTo(planet);
    }

    @Test
    void initialize_rover() {
        Planet planet = new Planet(4);
        RoverPosition position = new RoverPosition(0, 1);

        Rover rover = new Rover(planet, position, "N");

        assertThat(rover.getPlanet()).isEqualTo(planet);
        assertThat(rover.getPosition()).isEqualTo(position);
        assertThat(rover.getDirection().name()).isEqualTo("N");
    }

    @Test
    void initialize_rover_with_invalid_argument() {
        Planet planet = new Planet(2);
        RoverPosition position = new RoverPosition(10, 1);

        Rover rover = new Rover(planet, position, "INVALID");
        RoverPosition expectedPosition = new RoverPosition(0, 0);

        assertThat(rover.getPlanet()).isEqualTo(planet);
        assertThat(rover.getPosition()).isEqualTo(expectedPosition);
        assertThat(rover.getDirection().name()).isEqualTo("N");
    }

    @ParameterizedTest(name = "execute commands {0} and move or/and turn rover from [{1},{2},{3}] to [{4},{5},{6}]")
    @DisplayName("execute multiples commands")
    @MethodSource("getInputAndExpectedPosition_commands")
    void move_and_or_turn_rover_multiple_times(char[] commands, Integer initialX, Integer initialY, String initialDirection, Integer expectedX, Integer expectedY, String expectedDirection) {
        RoverPosition position = new RoverPosition(initialX, initialY);
        Planet planet = new Planet(40);
        Rover rover = new Rover(planet, position, initialDirection);

        rover.receives(commands);
        RoverPosition expectedPosition = new RoverPosition(expectedX, expectedY);

        assertThat(rover.getPosition()).isEqualTo(expectedPosition);
        assertThat(rover.getDirection().name()).isEqualTo(expectedDirection);
    }

    @ParameterizedTest(name = "move forward from [{0},{1},{2}] to [{3},{4},{5}]")
    @DisplayName("move forward")
    @MethodSource("getInputAndExpectedPosition_forward")
    void move_forward(Integer initialX, Integer initialY, String initialDirection, Integer expectedX, Integer expectedY, String expectedDirection) {
        RoverPosition position = new RoverPosition(initialX, initialY);
        Planet planet = new Planet(40);
        Rover rover = new Rover(planet, position, initialDirection);

        rover.receives(new char[]{'f'});
        RoverPosition expectedPosition = new RoverPosition(expectedX, expectedY);

        assertThat(rover.getPosition()).isEqualTo(expectedPosition);
        assertThat(rover.getDirection().name()).isEqualTo(expectedDirection);
    }

    @ParameterizedTest(name = "move forward from [{0},{1},{2}] to [{3},{4},{5}]")
    @DisplayName("move backward")
    @MethodSource("getInputAndExpectedPosition_backward")
    void move_backward(Integer initialX, Integer initialY, String initialDirection, Integer expectedX, Integer expectedY, String expectedDirection) {
        RoverPosition position = new RoverPosition(initialX, initialY);
        Planet planet = new Planet(40);
        Rover rover = new Rover(planet, position, initialDirection);

        rover.receives(new char[]{'b'});
        RoverPosition expectedPosition = new RoverPosition(expectedX, expectedY);

        assertThat(rover.getPosition()).isEqualTo(expectedPosition);
        assertThat(rover.getDirection().name()).isEqualTo(expectedDirection);
    }

    @ParameterizedTest(name = "turn on left from [{0},{1},{2}] to [{3},{4},{5}]")
    @DisplayName("turn left")
    @MethodSource("getInputAndExpectedPosition_turnLeft")
    void turnLeft(Integer initialX, Integer initialY, String initialDirection, Integer expectedX, Integer expectedY, String expectedDirection) {
        RoverPosition position = new RoverPosition(initialX, initialY);
        Planet planet = new Planet(40);
        Rover rover = new Rover(planet, position, initialDirection);

        rover.receives(new char[]{'l'});
        RoverPosition expectedPosition = new RoverPosition(expectedX, expectedY);

        assertThat(rover.getPosition()).isEqualTo(expectedPosition);
        assertThat(rover.getDirection().name()).isEqualTo(expectedDirection);
    }

    @ParameterizedTest(name = "turn on left from [{0},{1},{2}] to [{3},{4},{5}]")
    @DisplayName("turn right")
    @MethodSource("getInputAndExpectedPosition_turnRight")
    void turnRight(Integer initialX, Integer initialY, String initialDirection, Integer expectedX, Integer expectedY, String expectedDirection) {
        RoverPosition position = new RoverPosition(initialX, initialY);
        Planet planet = new Planet(40);
        Rover rover = new Rover(planet, position, initialDirection);

        rover.receives(new char[]{'r'});
        RoverPosition expectedPosition = new RoverPosition(expectedX, expectedY);

        assertThat(rover.getPosition()).isEqualTo(expectedPosition);
        assertThat(rover.getDirection().name()).isEqualTo(expectedDirection);
    }

    @ParameterizedTest(name = "move forward [{0},{1},{2}] to [{3},{4},{5}]")
    @DisplayName("move forward edge case")
    @MethodSource("getInputAndExpectedPosition_forward_edge")
    void move_forward_edge_case(Integer initialX, Integer initialY, String initialDirection, Integer expectedX, Integer expectedY, String expectedDirection) {
        RoverPosition position = new RoverPosition(initialX, initialY);
        Planet planet = new Planet(4);
        Rover rover = new Rover(planet, position, initialDirection);

        rover.receives(new char[]{'f'});
        RoverPosition expectedPosition = new RoverPosition(expectedX, expectedY);

        assertThat(rover.getPosition()).isEqualTo(expectedPosition);
        assertThat(rover.getDirection().name()).isEqualTo(expectedDirection);
    }

    @Test
    void detect_obstacle() {
        ObstaclePosition pos1 = new ObstaclePosition(0, 0);
        Obstacle o1 = new Obstacle(pos1);
        ObstaclePosition pos2 = new ObstaclePosition(1, 1);
        Obstacle o2 = new Obstacle(pos2);
        ObstaclePosition pos3 = new ObstaclePosition(2, 2);
        Obstacle o3 = new Obstacle(pos3);
        List<Obstacle> obstacles = List.of(o1, o2, o3);
        Planet planet = new Planet(4, obstacles);
        RoverPosition position = new RoverPosition(0, 1);
        Rover rover = new Rover(planet, position, "S");
        char[] commands = new char[]{'f', 'l', 'f', 'f'};

        rover.receives(commands);
        RoverPosition expectedPosition = new RoverPosition(1, 2);

        assertThat(rover.getPosition()).isEqualTo(expectedPosition);
        assertThat(rover.getDirection().name()).isEqualTo("E");
    }

    @Test
    void abort_the_commands_sequence() {
        ObstaclePosition pos1 = new ObstaclePosition(0, 0);
        Obstacle o1 = new Obstacle(pos1);
        ObstaclePosition pos2 = new ObstaclePosition(1, 1);
        Obstacle o2 = new Obstacle(pos2);
        ObstaclePosition pos3 = new ObstaclePosition(2, 2);
        Obstacle o3 = new Obstacle(pos3);
        List<Obstacle> obstacles = List.of(o1, o2, o3);
        Planet planet = new Planet(4, obstacles);
        RoverPosition position = new RoverPosition(0, 1);
        Rover rover = new Rover(planet, position, "S");
        char[] commands = new char[]{'f', 'l', 'f', 'f', 'r'};

        rover.receives(commands);
        RoverPosition expectedPosition = new RoverPosition(1, 2);

        assertThat(rover.getPosition()).isEqualTo(expectedPosition);
        assertThat(rover.getDirection().name()).isEqualTo("E");
    }

    private static Stream<Arguments> getInputAndExpectedPosition_forward() {
        return Stream.of(
                Arguments.of(0, 1, "N", 0, 0, "N"),
                Arguments.of(0, 3, "S", 0, 4, "S"),
                Arguments.of(0, 3, "E", 1, 3, "E"),
                Arguments.of(4, 3, "W", 3, 3, "W")
        );
    }

    private static Stream<Arguments> getInputAndExpectedPosition_backward() {
        return Stream.of(
                Arguments.of(0, 1, "N", 0, 2, "N"),
                Arguments.of(0, 1, "S", 0, 0, "S"),
                Arguments.of(1, 1, "E", 0, 1, "E"),
                Arguments.of(1, 1, "W", 2, 1, "W")
        );
    }

    private static Stream<Arguments> getInputAndExpectedPosition_turnLeft() {
        return Stream.of(
                Arguments.of(0, 0, "N", 0, 0, "W"),
                Arguments.of(0, 0, "W", 0, 0, "S"),
                Arguments.of(0, 0, "S", 0, 0, "E"),
                Arguments.of(0, 0, "E", 0, 0, "N")
        );
    }

    private static Stream<Arguments> getInputAndExpectedPosition_turnRight() {
        return Stream.of(
                Arguments.of(0, 0, "N", 0, 0, "E"),
                Arguments.of(0, 0, "E", 0, 0, "S"),
                Arguments.of(0, 0, "S", 0, 0, "W"),
                Arguments.of(0, 0, "W", 0, 0, "N")
        );
    }

    private static Stream<Arguments> getInputAndExpectedPosition_commands() {
        char[] unknown_command = new char[]{'a', 'c', 'd'};
        char[] valid_command = new char[]{'b', 'r', 'f', 'f'};
        char[] mixed_command = new char[]{'b', 'r', 'f', 'a', 't', 'f'};
        return Stream.of(
                Arguments.of(unknown_command, 0, 1, "N", 0, 1, "N"),
                Arguments.of(valid_command, 0, 1, "N", 2, 2, "E"),
                Arguments.of(mixed_command, 0, 2, "W", 1, 0, "N")
        );
    }

    private static Stream<Arguments> getInputAndExpectedPosition_forward_edge() {
        return Stream.of(
                Arguments.of(0, 0, "N", 0, 3, "N"),
                Arguments.of(3, 3, "S", 3, 0, "S"),
                Arguments.of(3, 3, "E", 0, 3, "E"),
                Arguments.of(0, 3, "W", 3, 3, "W")
        );
    }

}
