package org.example.obstacle;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ObstacleShould {
    @Test
    void initialize_position() {
        ObstaclePosition position = new ObstaclePosition(4,3);
        Obstacle obstacle = new Obstacle(position);

        assertThat(obstacle.getPosition()).isEqualTo(position);
    }
}
