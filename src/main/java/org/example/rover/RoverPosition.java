package org.example.rover;

import lombok.Getter;
import org.example.planet.Planet;
import org.example.position.Position;
import org.example.utils.Command;

import java.util.Optional;


@Getter
public class RoverPosition extends Position {
    private static final int BOTTOM_EDGE_VALUE = 0;

    public RoverPosition(int x, int y) {
        super(x, y);
    }

    public static Optional<RoverPosition> of(Planet planet, RoverPosition position) {
        if (position.isInvalidOn(planet)) return Optional.empty();
        return Optional.of(position);
    }

    public RoverPosition move(char command, Rover rover) {
        RoverPosition newPosition = rover.getPosition();
        switch (rover.getDirection()) {
            case N:
            case S:
                newPosition = updateRoverPositionY(rover, command);
                break;
            case E:
            case W:
                newPosition = updateRoverPositionX(rover, command);
                break;
        }
        return newPosition;
    }

    public boolean detectObstacle(char command, Rover rover) {
        RoverPosition nextPos = move(command, rover);
        return rover.getPlanet().obstacleAt(nextPos);
    }

    private boolean isOnTopEdgeForX(Integer edgeValue) {
        return getX() == edgeValue;
    }

    private boolean isOnBottomEdgeForX() {
        return getX() == BOTTOM_EDGE_VALUE;
    }

    private boolean isOnTopEdgeForY(Integer edgeValue) {
        return getY() == edgeValue;
    }

    private boolean isOnBottomEdgeForY() {
        return getY() == 0;
    }

    private RoverPosition updateRoverPositionY(Rover rover, char command) {
        return updateY(rover, command);
    }

    private RoverPosition updateRoverPositionX(Rover rover, char command) {
        return updateX(rover, command);
    }

    private RoverPosition updateY(Rover rover, char command) {
        if (roverMayEncounterBottomEdgeGrid(rover.getDirection(), command))
            return getNewY(isOnBottomEdgeForY(), rover.getPlanet().edge(), -1);
        return getNewY(isOnTopEdgeForY(rover.getPlanet().edge()), BOTTOM_EDGE_VALUE, 1);
    }

    private int addToY(int value) {
        return getY() + value;
    }

    private int addToX(int value) {
        return getX() + value;
    }

    private RoverPosition getNewY(boolean isOnEdge, int newValue, int increment) {
        if (isOnEdge) return new RoverPosition(getX(), newValue);
        return new RoverPosition(getX(), addToY(increment));
    }

    private RoverPosition updateX(Rover rover, char command) {
        if (roverMayEncounterRightEdgeGrid(rover.getDirection(), command))
            return getNewX(isOnTopEdgeForX(rover.getPlanet().edge()), BOTTOM_EDGE_VALUE, 1);
        return getNewX(isOnBottomEdgeForX(), rover.getPlanet().edge(), -1);
    }

    private RoverPosition getNewX(boolean isOnEdge, int newValue, int increment) {
        if (isOnEdge) return new RoverPosition(newValue, getY());
        return new RoverPosition(addToX(increment), getY());
    }

    private boolean roverMayEncounterRightEdgeGrid(Direction direction, char command) {
        return roverMoveForwardInEastDirection(direction, command) || roverMoveBackwardInWestDirection(direction, command);
    }

    private static boolean roverMayEncounterBottomEdgeGrid(Direction direction, char command) {
        return roverMoveForwardInNorthDirection(direction, command) || roverMoveBackwardInSouthDirection(direction, command);
    }

    private static boolean roverMoveBackwardInSouthDirection(Direction direction, char command) {
        return command == Command.MOVE_BACKWARD && direction == Direction.S;
    }

    private static boolean roverMoveForwardInNorthDirection(Direction direction, char command) {
        return command == Command.MOVE_FORWARD && direction == Direction.N;
    }

    private static boolean roverMoveBackwardInWestDirection(Direction direction, char command) {
        return command == Command.MOVE_BACKWARD && direction == Direction.W;
    }

    private static boolean roverMoveForwardInEastDirection(Direction direction, char command) {
        return command == Command.MOVE_FORWARD && direction == Direction.E;
    }
}
