package org.example.rover;

import lombok.Getter;
import org.example.planet.Planet;
import org.example.position.Position;
import org.example.utils.Command;

@Getter
public class Rover {
    private Planet planet;
    private RoverPosition position;
    private Direction direction;

    public Rover(RoverPosition position) {
        this.position = position;
    }

    public Rover(Planet planet) {
        this.planet = planet;
    }

    public Rover(Planet planet, RoverPosition position, String direction) {
        this.planet = planet;
        this.position = RoverPosition.of(planet, position).orElse(new RoverPosition(0, 0));
        this.direction = Direction.of(direction).orElse(Direction.N);
    }

    public void receives(char[] commands) {
        for (char command : commands) {
            if (obstacleAtNextPosition(command)) {
                reportObstacle(nextPosition(command), command);
                return;
            }
            execute(command);
        }
    }

    private RoverPosition nextPosition(char command) {
        return position.move(command, this);
    }

    private boolean obstacleAtNextPosition(char command) {
        return position.detectObstacle(command, this);
    }

    private void execute(char command) {
        switch (command) {
            case Command.MOVE_FORWARD:
            case Command.MOVE_BACKWARD:
                move(command);
                break;
            case Command.TURN_LEFT:
            case Command.TURN_RIGHT:
                turn(command);
                break;
        }
    }

    private void move(char command) {
        position = nextPosition(command);
    }

    private void turn(char command) {
        if (command == Command.TURN_LEFT) turnLeft();
        else turnRight();
    }

    private void turnLeft() {
        direction = direction.left();
    }

    private void turnRight() {
        direction = direction.right();
    }

    private void reportObstacle(Position obstaclePosition, char command) {
        System.out.println("Obstacle encounter at {" + obstaclePosition.getX() + "," + obstaclePosition.getY() + "} if execute command : '" + command + "' :");
        System.out.println("Rover abort sequence");
    }
}
