package org.example.rover;

import java.util.Optional;

public enum Direction {
    N {
        @Override
        Direction left() {
            return W;
        }

        @Override
        Direction right() {
            return E;
        }
    },
    E {
        @Override
        Direction left() {
            return N;
        }

        @Override
        Direction right() {
            return S;
        }
    },
    S {
        @Override
        Direction left() {
            return E;
        }

        @Override
        Direction right() {
            return W;
        }
    },
    W {
        @Override
        Direction left() {
            return S;
        }

        @Override
        Direction right() {
            return N;
        }
    };

    public static Optional<Direction> of(String direction) {
        try {
            return Optional.of(Direction.valueOf(direction));
        } catch (IllegalArgumentException e) {
            return Optional.empty();
        }
    }

    abstract Direction left();

    abstract Direction right();
}
