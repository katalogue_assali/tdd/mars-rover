package org.example.planet;

import lombok.Getter;
import org.example.obstacle.Obstacle;
import org.example.position.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class Planet {
    private final Integer size;
    private final int[][] grid;
    private final List<Obstacle> obstacles;

    public Planet(int size) {
        this.size = size;
        this.obstacles = new ArrayList<>();
        this.grid = new int[size][size];
    }

    public Planet(int size, List<Obstacle> obstacles) {
        this.size = size;
        this.obstacles = initializeObstacles(obstacles);
        this.grid = new int[size][size];
    }

    public Integer edge() {
        return size - 1;
    }

    public int countObstacles() {
        return obstacles.size();
    }

    public boolean obstacleAt(Position nextRoverPosition) {
        return obstacles.stream()
                .map(Obstacle::getPosition)
                .anyMatch(obstaclePosition -> obstaclePosition.equals(nextRoverPosition));
    }

    private List<Obstacle> initializeObstacles(List<Obstacle> obstacles) {
        return obstacles.stream()
                .filter(obstacle -> obstacle.getPosition().isValidOn(this))
                .collect(Collectors.toList());
    }
}
