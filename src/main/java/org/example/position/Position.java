package org.example.position;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.example.planet.Planet;

@Getter
@EqualsAndHashCode
public abstract class Position {
    private final int x;
    private final int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public boolean isInvalidOn(Planet planet) {
        return isBeyondEdge(planet);
    }

    private boolean isBeyondEdge(Planet planet) {
        return isBeyondEdgeX(planet) || isBeyondEdgeY(planet);
    }

    private boolean isBeyondEdgeY(Planet planet) {
        return y > planet.edge() || y < 0;
    }

    private boolean isBeyondEdgeX(Planet planet) {
        return x > planet.edge() || x < 0;
    }

    public boolean isValidOn(Planet planet) {
        return !isInvalidOn(planet);
    }
}
