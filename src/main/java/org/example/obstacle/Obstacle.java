package org.example.obstacle;

import lombok.Getter;

@Getter
public class Obstacle {
    private final ObstaclePosition position;

    public Obstacle(ObstaclePosition obstaclePosition) {
        this.position = obstaclePosition;
    }
}
