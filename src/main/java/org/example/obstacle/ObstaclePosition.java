package org.example.obstacle;

import lombok.Getter;
import org.example.position.Position;

@Getter
public class ObstaclePosition extends Position {

    public ObstaclePosition(int x, int y) {
        super(x, y);
    }
}
