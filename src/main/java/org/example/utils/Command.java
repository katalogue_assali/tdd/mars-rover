package org.example.utils;

public class Command {
    public static final char MOVE_FORWARD = 'f';
    public static final char MOVE_BACKWARD = 'b';
    public static final char TURN_LEFT = 'l';
    public static final char TURN_RIGHT = 'r';
}
